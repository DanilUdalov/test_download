FactoryGirl.define do
  factory :profile do
    first_name "MyString"
    last_name "MyString"
    username "MyString"
    birthday "MyString"
    user nil
  end
end
