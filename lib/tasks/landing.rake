namespace :landing do
  desc "Delete converted older than 1 days"
  task delete_converted_1_day_old: :environment do
    Converted.where(['created_at < ?', 1.days.ago]).destroy_all
  end

end
