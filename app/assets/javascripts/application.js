//= require jquery
//= require jquery_ujs
//= require rails-ujs
//= require turbolinks
//= require bootstrap-sprockets
//= require nprogress
//= require nprogress-turbolinks
//= require angular
//= require_tree .
$(document).on('turbolinks:load', function() {
  $(function() {
    $(".message" ).fadeOut(3000);
  }); 
});
$(document).ready(function() {
  NProgress.start();
});
$(document).on('turbolinks:render', function() {
  NProgress.done();
  NProgress.remove();
});
