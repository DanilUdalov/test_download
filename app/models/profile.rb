class Profile < ApplicationRecord
  belongs_to :user

  def full_name
    "#{first_name} #{last_name}"
  end

  def age
  	Date.today.year - birthday.year
  end
end
