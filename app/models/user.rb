class User < ApplicationRecord
  has_secure_password
  has_one :profile, dependent: :destroy

  validates_uniqueness_of :email
  validates_presence_of :email
  validates_presence_of :password, on: :create  

  before_create :build_profile

  def name_space
    if profile.full_name.delete(' ').length.zero?
      email
    else
      profile.full_name
    end
  end

  def user_active
    if active?
      'active'
    else
      'banned'
    end
  end
end
