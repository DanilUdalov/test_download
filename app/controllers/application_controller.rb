class ApplicationController < ActionController::Base
  protect_from_forgery with: :null_session

  helper_method :current_user
  before_action :active?
  before_action :set_locale

  def active?
    return unless current_user.present? && current_user.active == false
    session[:user_id] = nil
    redirect_to root_path, notice: 'You banned!'
  end

  private

  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end

  def set_locale
    if cookies[:educator_locale] && I18n.available_locales.include?(cookies[:educator_locale].to_sym)
      l = cookies[:educator_locale].to_sym
    else
      begin
        country_code = request.location.country_code
        if country_code
          country_code = country_code.downcase.to_sym
          # use russian for CIS countries, english for others
          [:ru, :kz, :ua, :by, :tj, :uz, :md, :az, :am, :kg, :tm].include?(country_code) ? l = :ru : l = :en
        else
          l = I18n.default_locale # use default locale if cannot retrieve this info
        end
      rescue
        l = I18n.default_locale
      ensure
        cookies.permanent[:educator_locale] = l
      end
    end
    I18n.locale = l
  end

  # def set_locale
  #   I18n.locale = params[:locale] if params[:locale].present?
  # end

  # def default_url_options(options = {})
  #   {locale: I18n.locale}
  # end
end
