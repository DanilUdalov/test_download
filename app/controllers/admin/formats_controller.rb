class Admin::FormatsController < AdminsController
  def index
    @format = Format.new
    @formats = Format.all
  end

  def create
    @format = Format.new(format_params)
    if @format.save
      redirect_back fallback_location: root_path, notice: 'Format was successfully added'
    else
      render :new
    end
  end

  def destroy
    @format = Format.find(params[:id])
    @format.destroy
  end

  private

  def format_params
    params.require(:format).permit(:name, :types)
  end
end
