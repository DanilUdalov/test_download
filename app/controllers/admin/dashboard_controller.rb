class Admin::DashboardController < AdminsController
  before_action :destroy_converted

  def index
    @connects = Connect.all
    @converts = Converted.all
  end

  private

  def destroy_converted
  	Converted.where(['created_at < ?', 1.days.ago]).destroy_all
  end
end
