class Admin::UsersController < AdminsController
  before_action :user_resource, only: %i[destroy active_user]

  def index
    @users = User.order(:id)
  end

  def destroy
    @user.destroy!
    redirect_back fallback_location: root_path
  end

  def active_user
    @user.update(active: !@user.active)
    redirect_back fallback_location: root_path
  end

  private

  def user_resource
    @user = User.find(params[:id])
  end
end
