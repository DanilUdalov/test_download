class Admin::SessionsController < AdminsController
  skip_before_action :require_admin, only: %i[new create]

  def create
    if params[:email] == 'admin@mail.ru' && params[:password] == 'qazwsx123'
      session[:admin] = true
      redirect_to admin_dashboard_index_path, notice: 'Welcome admin'
    else
      render :new
    end
  end

  def destroy
    session[:admin] = false
    redirect_to admin_root_path
  end
end
