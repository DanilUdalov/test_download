require 'socket'

class LandingController < ApplicationController
  before_action :params_present, only: [:create]
  # before_action :converted_end, only: [:download]

  def create
    url = params[:url]
    form = params[:format]
    quality = params[:quality]
    link = LinkThumbnailer.generate(url)
    name = link.title

    # request.original_url

    # redirect_to error_path if Connect.where(url: request.url).count >= 5

    # ip = Socket.ip_address_list.detect{|intf| intf.ipv4_private?}
    # ip_adress = ip.ip_address if ip

    # Connect.create(ip: ip_adress, url: request.url)

    YoutubeDL.download url, output: name
    movie = FFMPEG::Movie.new(name)
    movie.transcode("tmp/#{name}.#{form}", resolution: quality) { |prog| puts prog }
    File.delete(name.to_s)
    Converted.create(path: "tmp/#{name}.#{form}")
    # Connect.last.destroy!

    redirect_to down_path, notice: "The video #{name}.#{form} succesifal converted"
  end

  def download
    send_file Converted.last.path
  end

  private

  def params_present
    if params[:format].nil?
      redirect_to root_path, notice: 'Select a format video'
    end
  end

  def converted_end
    File.delete(Converted.last.path)
    Converted.last.destroy
  end
end
