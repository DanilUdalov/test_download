require 'socket'

class Api::V2::VideosController < ApplicationController
  before_action :api_connect

  def index; end

  private

  def api_connect
    # binding.pry
    request.original_url
    ip = Socket.ip_address_list.detect(&:ipv4_private?)
    ip_adress = ip.ip_address if ip
    Connect.create(ip: ip_adress, url: request.url)
  end
end
