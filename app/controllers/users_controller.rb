class UsersController < ApplicationController
  before_action :current_resourse,
                only: %i[show edit update]

  def new
    @user = User.new
  end

  def show; end

  def create
    @user = User.new(user_params)
    if @user.save
      session[:user_id] = @user.id
      redirect_to edit_user_profile_path(@user), notice: 'Thank you for singning up!'
    else
      render 'new'
    end
  end

  def update
    if @user.update(user_params)
      redirect_to @user
    else
      render 'edit'
    end
  end

  def edit; end

  private

  def current_resourse
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation, :active)
  end
end
