source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?('/')
  "https://github.com/#{repo_name}.git"
end

gem 'pry'
gem 'rubocop'

gem 'active_model_serializers'
gem 'carrierwave'
gem 'jquery-rails'
gem 'less-rails'
gem 'link_thumbnailer'
gem 'paperclip-ffmpeg'
gem 'paperclip-av-transcoder'
gem 'pg', '~> 0.18'
gem 'puma', '~> 3.7'
gem 'rails', '~> 5.1.2'
gem 'sass-rails', '~> 5.0'
gem 'slim-rails'
gem 'streamio-ffmpeg'
gem 'therubyracer'
gem 'uglifier', '>= 1.3.0'
gem 'viddl-rb'
gem 'youtube-dl.rb'
gem 'progress_bar'
gem 'ipaddress'
gem "bcrypt-ruby", :require => "bcrypt"
gem 'whenever', :require => false
gem 'angularjs-rails'
gem 'rails-i18n', '~> 5.0.0'
gem 'geocoder'

gem 'bootstrap-sass', '~> 3.3.6'
gem 'coffee-rails', '~> 4.2'
gem 'figaro'
gem 'font-awesome-rails'
gem 'jbuilder', '~> 2.5'
gem 'jquery-rails'
gem 'turbolinks', '~> 5'
gem "nprogress-rails"
gem 'faraday'

group :development, :test do
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  gem 'capybara', '~> 2.13'
  gem 'factory_girl_rails'
  gem 'faker'
  gem 'rails-controller-testing'
  gem 'rspec-rails'
  gem 'selenium-webdriver'
  gem 'shoulda-matchers'
end

group :development do
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'web-console', '>= 3.3.0'
end

gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
