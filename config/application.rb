require_relative 'boot'

require 'rails/all'

Bundler.require(*Rails.groups)

module TestConvert
  class Application < Rails::Application
    config.load_defaults 5.1

    config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :ru
    config.i18n.available_locales = [:en, :ru]
    config.time_zone = 'Kyiv'
    config.i18n.default_locale = 'en'
    config.i18n.fallbacks = [:en]
    config.assets.paths << Rails.root.join('vendor', 'assets', 'components')
  end
end
