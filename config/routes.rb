Rails.application.routes.draw do
  # scope "(:locale)", locale: /en|ru/, defaults: {locale: "en"} do
  # scope ":locale", locale: /#{I18n.available_locales.join("|")}/, defaults: {locale: "en"} do
  resources :sessions
  get 'signup', to: 'users#new', as: 'signup'
  get 'login', to: 'sessions#new', as: 'login'
  get 'logout', to: 'sessions#destroy', as: 'logout'
  
  resources :users do
    resource :profile, only: [:show, :edit, :update]
  end
  get 'tutorial/index'
  
  root 'landing#index'

  get '/change_locale/:locale', to: 'settings#change_locale', as: :change_locale

  get 'error', to: 'landing#404', as: 'error'
  
  post 'create', to: 'landing#create', as: 'create'
  get 'download', to: 'landing#download', as: 'download'
  get 'down', to: 'landing#down', as: 'down'
  
  namespace :admin do
    root 'sessions#new'
    get 'logout', to: 'sessions#destroy'
    get 'login', to: 'sessions#new', as: 'login'
    get '/change_locale/:locale', to: 'settings#change_locale', as: :change_locale
  
    resources :sessions, only: [:new, :create, :destroy]
    resources :dashboard, only: [:index]
    resources :formats
    resources :users do
      get :active_user, on: :member
    end
  end
  
  namespace :api do
    namespace :pcs1 do
      resources :videos, only: [:index]
    end
  end
  # end
end
