class CreateConverteds < ActiveRecord::Migration[5.1]
  def change
    create_table :converteds do |t|
      t.string :path

      t.timestamps
    end
  end
end
