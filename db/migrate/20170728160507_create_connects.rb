class CreateConnects < ActiveRecord::Migration[5.1]
  def change
    create_table :connects do |t|
      t.string :ip
      t.string :url

      t.timestamps
    end
  end
end
